section .text 

global suma

suma:
    push   ebp
    mov    ebp, esp
    %idefine    a    [ebp+8]
    %idefine    n    [ebp+12]

    mov edx, a
    mov ecx, n
    mov eax, 0

    lp:
        add eax, [edx + 4*(ecx-1)]
        loop lp

leave
ret
