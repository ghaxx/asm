#include <stdio.h>
#include <malloc.h>

int suma (int* a, int b);

int main() {
    int n = 0, i = 0;
    int *a;
    printf("n = ");
    scanf("%d", &n);

    a = malloc(n * sizeof (int));

    while (++i <= n) {
        a[i-1] = i*i;
        printf("%d ", a[i-1]);
    }

    printf("\nsuma = %d\n", suma(a, n));
    return 0;
}
