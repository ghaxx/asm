#include <stdio.h>

int suma(int a, int b); 

int main() {
    int n = 0, s = 0, i = 0;
    printf("n = ");
    scanf("%d", &n);
    while (++i <= n)
        s = suma(s, i);
    printf("suma = %d\n", s);
    return 0;
}
