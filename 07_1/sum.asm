section .text 

global suma 
 
suma:
    push   ebp
    mov    ebp, esp 

    %idefine    a    [ebp+8]
    %idefine    b    [ebp+12]

    mov    eax, a
    add    eax, b

leave
ret
