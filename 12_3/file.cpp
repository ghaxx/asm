#include <cstdio>
#include <cstdlib>
#include <string>
#include <time.h>

using namespace std;

void sort_c(int n, double * tab) {
  
}


void try_func(string name, void (*f)(int, double*), int rozmiar, int liczba_powtorzen, double *tab) {
   int min = 0, max = 0;
   clock_t start, stop;
   start = clock();
   for(int i=0; i<liczba_powtorzen; i++){
      (*f)(rozmiar, tab);
   }
   printf("%s\n", name.c_str());
   printf(" min = %d    max = %d\n", min, max); 
   stop = clock();
   printf(" time = %f  ( %d cykli)\n", (stop - start)*1.0/CLOCKS_PER_SEC, (int)(stop - start));
}

int main(){
   const int rozmiar = 2000000;
   const int liczba_powtorzen = 150; 
   double tab[rozmiar] = {1, 3, 3, -65, 3, 123, 4, 32, 342, 22, 11, 32, 44, 12, 324, 43}; 

   for(int i=0; i<rozmiar; i++){
      tab[i] = ((double) rand() / RAND_MAX) * 10.0;
   }

   try_func("C", sort_c, rozmiar, liczba_powtorzen, tab);

   for(int i=0; i<rozmiar; i++){
      printf("%4.4f ", tab[i]);
   }
   printf("\n");
   return 0;
}