%define n   dword [ebp+8]
%define tab dword [ebp+12]
%define max dword [ebp+16]
%define min dword [ebp+20]

segment .text

global mm
mm:

push ebp
mov  ebp, esp
push ebx

    ; START
    mov ecx, n
    mov [s], ecx
    mov dword [i], 0

    mov ebx, tab
    mov ebx, [ebx]

    mov dword[l_max], ebx
    mov dword[l_min], ebx

    max_min:
        mov ecx, [s]
        mov edx, tab
        mov ebx, [edx + ecx*4 - 4]
        mov eax, [l_max]
        cmp eax, ebx
        jg not_max
        mov [l_max], ebx
        not_max:

        mov eax, [l_min]
        cmp eax, ebx
        jl not_min
        mov [l_min], ebx
        not_min:

        mov eax, [s]
        dec eax
        mov [s], eax
        cmp eax, 0
        jg  max_min

    ;     END
        

    mov ebx, [l_min]
    mov eax, min
    mov dword[eax], ebx
    mov ebx, [l_max]
    mov eax, max
    mov dword[eax], ebx


mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret

section .bss
    i       resd  10
    s       resd  10
    l_max   resd  2
    l_min   resd  2
