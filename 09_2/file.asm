; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define a dword [ebp+8]
%define b dword [ebp+12]
%define c dword [ebp+16]

segment .data
zero dw 0

segment .text

global kopiuj
kopiuj:

push ebp
mov ebp, esp
sub esp, 16            
push ebx               

mov esi, a
mov edi, b
mov ecx, c
rep movsd

mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret

global zeruj
zeruj:

push ebp
mov ebp, esp
sub esp, 16          
push ebx               

mov eax, 0
mov edi, a
mov ecx, b
rep stosd

pop ebx
mov esp, ebp
pop ebp
ret