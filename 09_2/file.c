#include <stdio.h>

// kopiuje n liczb typu int z zrodla do celu 
void kopiuj(int * cel, int * zrodlo, unsigned int n);

// zeruje tablice liczb typu int o rozmiarze n
void zeruj(int * tablica, unsigned int n);

int main() {
    int i, n;
    printf("n = ");
    scanf("%d", &n);

    int src[n], dst[n];
    
    for (i = 0; i < n; i++) {
        printf("a[%d] = %d\n", i, i);
        src[i] = i;
    }

    kopiuj(src, dst, n);
    printf("kopiuj\n");
    for (i = 0; i < n; i++) {
        printf("dst[%d] = %d\n", i, dst[i]);
    }

    zeruj(dst, n);
    printf("zeruj\n");
    for (i = 0; i < n; i++) {
        printf("dst[%d] = %d\n", i, dst[i]);
    }
}
