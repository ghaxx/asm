SECTION .text
%include "inc.asm"

extern printf
global main

main:
    jmp go
    exit

go:
    ; push ebp
    ; mov ebp,esp
    ; push ebx
    ; push esi
    ; push edi        ; stuff before this for glibc compatibility 

    mov eax, 13
    int 80h

    mov edx, 0    
    mov ebx, 60 
    div ebx     ; Divides time by 60. EDX = seconds%60 and EAX = minutes
    push edx

    mov edx, 0    
    mov ebx, 60 
    div ebx     ; Divides minutes by 60. EDX = mnutes%60 and EAX = hours
    push edx

    mov edx, 0    
    mov ebx, 24 
    div ebx     ; hours % 24   
    push edx 

    push DateFormat
    call printf
    ;add esp,8   

    pop eax
    pop eax
    pop eax
    pop eax
    ; jmp go

    ; pop edi         ; stuff after this for glibc compatibility
    ; pop esi
    ; pop ebx
    ; mov esp,ebp
    ; pop ebp
    

SECTION .data
    DateFormat    db "%02d:%02d:%02d", 10, 0 ;this is a c string so is null terminated
