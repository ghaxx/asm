#include <stdio.h>

extern int calc_y(double x, double a, double b, double c, double d, double *y);

int main() {
    double x = 1.2, a = 2.3, b = 3.4, c = 4.5, d = 5.6, y;
//    printf("x a b c d = ");
//    scanf("%lf %lf %lf %lf %lf", &x, &a, &b, &c, &d);
    calc_y(x, a, b, c, d, &y);

    printf("%g*%g^3 + %g*%g^2 + %g*%g + %g = %.6g\n", a,x, b,x, c,x, d, y);
}
