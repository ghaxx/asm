; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define x qword [ebp+8]
%define a qword [ebp+16]
%define b qword [ebp+24]
%define c qword [ebp+32]
%define d qword [ebp+40]
%define y dword [ebp+48]

segment .data
two dw 2

segment .text
global calc_y
calc_y:

push ebp
mov ebp, esp
push ebx

fld x
fld a
fmulp st1
fld b
faddp st1
fld x
fmulp st1
fld c
faddp st1
fld x
fmulp st1
fld d
faddp st1

mov ebx, y
fstp qword [ebx]

mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret
