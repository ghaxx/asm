%include "inc.asm"

section .text
extern printf

global main

main:
    c_start
    mov eax, [n]
    easy_println "eax = %d", eax

    mov ecx, 1
    mov [tmp], ecx

    mov eax, [n]
    next:
        mov ecx, [tmp]
        inc ecx
        mov [tmp], ecx
        mov edx, 0
        mov ebx, 10
        div ebx     ; EAX / EBX. in: EAX, out: EAX(1) = EAX(2)*EBX + EDX   
        push edx
        cmp eax, 0
        jne next
    
    ;mov dl, 'a'
    ;int 02h ; powoduje u mnie segmentation fault, wiec uzyje printf

    print:
        push format
        call printf
        pop eax
        pop eax ; add esp, 8 nie dziala

        mov ecx, [tmp]
        dec ecx
        mov [tmp], ecx
        loop print

    easy_println "" ; makro
    c_exit


section data:
    format  db "%d", 0
    n       dw  30892

section .bss
    tmp     resw 5