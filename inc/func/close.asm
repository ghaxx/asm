%macro close_file 0

	mov	rax, sys_close
	mov	rdi, rbx
	syscall

%endmacro
