%macro  open 3

	mov	rax, sys_open
	mov	rdi, %1
	mov	rsi, %2	
	mov	rdx, %3	
	syscall

%endmacro

%macro  open 1

	open %1, O_CREAT|O_RDWR, 0644o ; create if doesnt exist, truncate content | permissions

%endmacro

%macro  open_by_name 1
    [section .data]

    %%str       db     %1, 0

    __SECT__

	open %%str, O_CREAT|O_RDWR, 0644o ; create if doesnt exist, truncate content | permissions

%endmacro