%macro  write 1
    [section .data]

    %%str       db      %1
    %%endstr    equ    $ - %%str

    __SECT__

	mov	sys_write_fn_number,   sys_write
	mov	sys_write_descriptor,  1
	mov	sys_write_address,     %%str
	mov	sys_write_length,      %%endstr
	syscall

%endmacro

%macro  writeln 1
    [section .data]

    %%str       db      %1, 10, 0
    %%endstr    equ     $ - %%str

    __SECT__

	mov	sys_write_fn_number,   sys_write
	mov	sys_write_descriptor,  1
	mov	sys_write_address,     %%str
	mov	sys_write_length,      %%endstr
	syscall

%endmacro

%macro  write 2

	mov	sys_write_fn_number,   sys_write
	mov	sys_write_descriptor,  1
	mov	sys_write_address,     %1
	mov	sys_write_length,      %2
	syscall

%endmacro

%macro  write_file 2
    [section .data]

    %%str       db      %1
    %%endstr    equ     $ - %%str

    __SECT__

	mov	sys_write_fn_number,   sys_write
	mov	sys_write_descriptor,  %2
	mov	sys_write_address,     %%str
	mov	sys_write_length,      %%endstr
	syscall

%endmacro

%macro  write 3

	mov	sys_write_fn_number,   sys_write
	mov	sys_write_descriptor,  %3
	mov	sys_write_address,     %1
	mov	sys_write_length,      %2
	syscall

%endmacro

%macro write_number 2
    push %1
    ;push rbp
    mov rbp, rsp
    mov sys_write_address, rbp
    ;add sys_write_address, 8
    add byte [sys_write_address], '0'
    write sys_write_address, %2
    pop sys_write_fn_number
%endmacro