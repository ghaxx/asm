%macro  read 2 ; var, length

	mov	rax, sys_read
	mov	rdi, 0		; stdin
	mov	rsi, %1
	mov	rdx, %2
	syscall

%endmacro