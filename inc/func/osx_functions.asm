%define	entry_point	start

%define	sys_read	0x2000003
%define	sys_write	0x2000004
%define	sys_exit	0x2000001
%define	sys_open	0x2000005
%define	sys_close	0x2000006
%define sys_time	0x2000013

%define O_CREAT 	0x0200
%define O_WRONLY 	0x0001
%define O_RDWR		0x0002