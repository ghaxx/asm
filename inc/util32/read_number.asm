; returns in dx
%macro read_new_number 2
    [section .rss]
    %2  resw 10

    __SECT__
    mov ebx, 0         ; ebx = adres tablicy array1
    mov ecx, 10

    mov word[%2], 0
    %%start:
        mov al, [%1+ebx]           ; al = *ebx
        cmp eax, 48
        jl %%end
        cmp eax, 58
        jg %%end

        sub eax, '0'
        mov ecx, eax
        mov ebx, [%2]
        mov eax, 10
        mul ebx

        add eax, ecx
        mov [%2], eax
        add ebx, 1                 ; bx++
    jmp %%start
    %%end:
%endmacro

%macro read_number 2
    mov ebx, 0         ; ebx = adres tablicy array1
    mov ecx, 10

    mov eax, 0
    mov [%2], eax

    mov word[%2], 0
    %%start:
        xor eax, eax
        mov al, [%1+ebx]           ; al = *ebx
        cmp eax, 48
        jl %%end
        cmp eax, 58
        jg %%end

        sub eax, '0'
        mov ecx, eax
        mov ebx, [%2]
        mov eax, 10
        mul ebx

        add eax, ecx
        mov [%2], eax
        add ebx, 1                 ; bx++
    jmp %%start
    %%end:

%endmacro