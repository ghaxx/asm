%macro  open 3

	mov	eax, sys_open
	mov	ebx, %1
	mov	ecx, %2	
	mov	edx, %3	
	int 80h

%endmacro

%macro  open 1

	open %1, O_CREAT|O_RDWR, 0644o ; create if doesnt exist, truncate content | permissions

%endmacro

%macro  open_by_name 1
    [section .data]

    %%str       db     %1, 0

    __SECT__

	open %%str, O_CREAT|O_RDWR, 0644o ; create if doesnt exist, truncate content | permissions

%endmacro