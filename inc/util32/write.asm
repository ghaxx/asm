%macro  write 1
    [section .data]

    %%str       db      %1
    %%endstr    equ    $ - %%str

    __SECT__

	mov	eax, sys_write
	mov	ebx, 1
	mov	ecx, %%str
	mov	edx, %%endstr
	int 80h
%endmacro

%macro  writeln 1
    [section .data]

    %%str       db      %1, 10, 0
    %%endstr    equ    $ - %%str

    __SECT__

	mov	eax, sys_write
	mov	ebx, 1
	mov	ecx, %%str
	mov	edx, %%endstr
	int 80h
%endmacro

%macro  write 2
	mov	eax, sys_write
	mov	ebx, 1
	mov	ecx, %1
	mov	edx, %2
	int 80h
%endmacro

%macro  write_file 2
    [section .data]

    %%str       db      %1
    %%endstr    equ    $ - %%str

    __SECT__

	mov	eax, sys_write
	mov	ebx, %2
	mov	ecx, %%str
	mov	edx, %%endstr
	int 80h
%endmacro

%macro  write 3
	mov	eax, sys_write
	mov	ebx, %3
	mov	ecx, %1
	mov	edx, %2
	int 80h
%endmacro

%macro write_number 2
    push    %1
    ;push   ebp
    mov     ebp, esp
    mov     esi, ebp
    ;add    esi, 16
    add     byte [esi], '1'
    write   esi, %2
    pop     eax
%endmacro