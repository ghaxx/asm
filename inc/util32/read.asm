%macro  read 2 ; var, length

	mov	eax, sys_read
	mov	ebx, 0		; stdin
	mov	ecx, %1 ; buf
	mov	edx, %2 ; 10
	int 80h

%endmacro