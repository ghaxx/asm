%macro  easy_print 1
    [section .data]
    %%str       db      %1, 0

    __SECT__

    push    %%str
    call    printf
    add esp, 4
%endmacro

%macro  easy_print 2
    [section .data]
    %%str       db      %1, 0

    __SECT__

    push %2
    push %%str
    call printf
    add esp, 8
%endmacro

%macro  easy_println 1
    [section .data]
    %%str       db      %1, 10, 0

    __SECT__

    push    %%str
    call    printf
    add esp, 4
%endmacro

%macro  easy_println 2
    [section .data]
    %%str       db      %1, 10, 0

    __SECT__

    push %2
    push %%str
    call printf
    add esp, 8
%endmacro

%macro  easy_println 3
    [section .data]
    %%str       db      %1, 10, 0

    __SECT__

    push %3
    push %2
    push %%str
    call printf
    add esp, 12
%endmacro

%macro  easy_println 4
    [section .data]
    %%str       db      %1, 10, 0

    __SECT__

    push %4
    push %3
    push %2
    push %%str
    call printf
    add esp, 16
%endmacro