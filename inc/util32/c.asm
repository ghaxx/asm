%macro c_start 0
    push    ebp
    mov     ebp, esp
    ;push    ebx
    ;push    esi
    ;push    edi
%endmacro

%macro c_exit 0
    ;pop     edi
    ;pop     esi
    ;pop     ebx
    mov     esp, ebp
    pop     ebp
    ret 
%endmacro
