%define	entry_point	_start

%define	sys_read	3
%define	sys_write	4
%define	sys_exit	1
%define	sys_open	5
%define	sys_close	6
%define sys_time	13

%define O_CREAT 	00000100
%define O_WRONLY 	00000001
%define O_RDWR		00000002
