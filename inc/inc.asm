%ifidn __OUTPUT_FORMAT__, elf64
%include "inc/util/linux_functions.asm"
%include "inc/util/64bit_registers.asm"
%include "inc/util/_all.asm"

%elifidn __OUTPUT_FORMAT__, elf
%include "inc/util32/32bit_registers.asm"
%include "inc/util32/linux_functions.asm"
%include "inc/util32/_all.asm"

%elifidn __OUTPUT_FORMAT__, macho64
%include "inc/util/osx_functions.asm"
%include "inc/util32/32bit_registers.asm"
%include "inc/util/_all.asm"

%elifidn __OUTPUT_FORMAT__, macho32
%include "inc/util32/osx_functions.asm"
%include "inc/util32/64bit_registers.asm"
%include "inc/util32/_all.asm"

%else

%include "inc/util32/32bit_registers.asm"
%include "inc/util32/linux_functions.asm"
%include "inc/util32/_all.asm"
%endif
