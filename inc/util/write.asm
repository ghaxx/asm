%macro  write 1
    [section .data]

    %%str       db      %1
    %%endstr    equ    $ - %%str

    __SECT__

	mov	rax, sys_write
	mov	rdi, 1
	mov	rsi, %%str
	mov	rdx, %%endstr
	syscall

%endmacro

%macro  writeln 1
    [section .data]

    %%str       db      %1, 10, 0
    %%endstr    equ    $ - %%str

    __SECT__

	mov	rax, sys_write
	mov	rdi, 1
	mov	rsi, %%str
	mov	rdx, %%endstr
	syscall

%endmacro

%macro  write 2

	mov	rax, sys_write
	mov	rdi, 1
	mov	rsi, %1
	mov	rdx, %2
	syscall

%endmacro

%macro  write_file 2
    [section .data]

    %%str       db      %1
    %%endstr    equ    $ - %%str

    __SECT__

	mov	rax, sys_write
	mov	rdi, %2
	mov	rsi, %%str
	mov	rdx, %%endstr
	syscall

%endmacro

%macro  write 3

	mov	rax, sys_write
	mov	rdi, %3
	mov	rsi, %1
	mov	rdx, %2
	syscall

%endmacro

%macro write_number 2
    push %1
    ;push rbp
    mov rbp, rsp
    mov rsi, rbp
    ;add rsi, 8
    add byte [rsi], '0'
    write rsi, %2
    pop rax
%endmacro