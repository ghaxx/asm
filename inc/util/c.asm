%macro c_start 0
    push    rbp
    mov     rbp, rsp
    ;push    ebx
    ;push    esi
    ;push    edi
%endmacro

%macro c_exit 0
    ;pop     edi
    ;pop     esi
    ;pop     ebx
    mov     rsp, rbp
    pop     rbp
    ret 
%endmacro
