%define	entry_point	_start

%define	sys_read	0
%define	sys_write	1
%define	sys_exit	60
%define	sys_open	2
%define	sys_close	3
%define sys_time	201

%define O_CREAT 	00000100
%define O_WRONLY 	00000001
%define O_RDWR		00000002
