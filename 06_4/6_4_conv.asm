section .text
%include "inc/inc.asm"
extern printf

global main

main:
    c_start

    read buf, 10
    read_number buf, n

    mov eax, [n]
    easy_println "eax = %d", eax

    mov eax, 2
    push 2
    call conv
    add esp, 4

    mov eax, 16
    push 16
    call conv
    add esp, 4

    mov eax, 8
    push 8
    call conv
    add esp, 4

    c_exit

conv:
    section .bss
    base    resw 2

    section .text
    mov eax, [esp + 4]
    ddd:
    mov [base], eax

    mov ecx, 1
    mov [tmp], ecx

    mov eax, [n]
    next2:
        mov ecx, [tmp]
        inc ecx
        mov [tmp], ecx
        mov edx, 0
        mov ebx, [base]
        div ebx     ; EAX / EBX. in: EAX, out: EAX(1) = EAX(2)*EBX + EDX   
        mov edx, [digit + edx]
        push edx
        cmp eax, 0
        jne next2
    
    ;mov dl, "a"
    ;int 02h ; powoduje u mnie segmentation fault, wiec uzyje printf

    print2:
        push format
        call printf
        pop eax
        pop eax ; add esp, 8 nie dziala

        mov ecx, [tmp]
        dec ecx
        mov [tmp], ecx
        loop print2

    easy_println "" ; makro
    ret


section data:
    digit   db "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"
    format  db "%c", 0

section .bss
    tmp     resw 5
    buf     resw 8
    n       resw 8
