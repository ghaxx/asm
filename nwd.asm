section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    ; print data
    mov     esi, 4000
    mov     edi, 6996
    push    esi
    push    edi
    push    InputFormat
    call    printf
    pop     edx
    jmp     check

check:
    mov     edx, 0
    mov     eax, esi
    mov     ebx, edi
    div     ebx

    cmp     edx, 0
    je      done

    mov     esi, ebx
    mov     edi, edx
    push    esi
    push    edi
    push    InputFormat
    call    printf
    jmp     check

done:
    push    edi
    push    OutputFormat
    call    printf
    exit

section .data
    InputFormat     db  "nwd(%d, %d) = ", 0
    OutputFormat    db  "%d",10, 0

