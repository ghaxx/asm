section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    c_start

    write "podaj ilosc liczb: "
    read buf, 10
    read_number buf, s, e0


    mov word[i], 0
    read_numbers:
        mov ebx, [i]
        easy_println "Liczba %d: ", ebx
        read buf, 10
        read_number buf, p, e1
        mov eax, [p]
        mov ebx, [i]
        mov [arr+ebx], eax
        easy_println "arr %d -> %d", ebx, eax

        mov ebx, [i]        ; increment counter
        inc ebx
        mov [i], ebx
        cmp ebx, [s]
        jne read_numbers


    mov ebx, arr            ; ebx = adres tablicy
    mov dx, 0               ; dx będzie zawierać sumę
    mov ah, 0               ; zerujemy ah, bo w sumowaniu uczestniczy rejestr ax
    mov ecx, [s]

    lp:
    mov al, [ebx]           ; al = *ebx
    add dx, ax              ; dx += ax (nie al!)
    inc ebx                 ; bx++
    loop lp

    easy_println "sum = %d", dx

    c_exit


section data:
    array1  db 6, 5, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1

section .bss
    size    resb 2
    buf     resb 1000
    arr     resb 1000
    p       resw 8
    i       resw 8
    s       resw 100
