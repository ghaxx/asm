section .text
%include "inc.asm"

extern printf
global main

main:
    mov ebx, 1
    mov ecx, search_from
    jmp check

check:
    mov eax, ecx
    cmp eax, 1
    je  is_not_prime

    mov edx, 0    
    inc ebx
    cmp ebx, eax
    jge is_prime

    div ebx     ; Divides eax / ebx => ebx*eax + edx
    cmp edx, 0
    je is_not_prime
    jmp check

is_prime:
    mov     ebx, ecx
    push    ecx
    push    NumberFormat
    call    printf
    jmp     next

is_not_prime:
    mov     ebx, ecx
    push    ecx
    push    NumberFormat2
    call    printf
    jmp     next

next:
    mov     ecx, ebx
    add     ecx, 1
    mov     ebx, 1
    cmp     ecx, search_to
    jg      done
    jmp     check

done:
    exit

section .data
    search_from         equ 1
    search_to           equ 100
    NumberFormat        db "+++ %6d", 10, 0 ;this is a c string so is null terminated
    NumberFormat2       db "--- %6d", 10, 0 ;this is a c string so is null terminated

section .bss
    buf:    resb    1

