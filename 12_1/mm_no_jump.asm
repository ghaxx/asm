%define n   dword [ebp+8]
%define tab dword [ebp+12]
%define max dword [ebp+16]
%define min dword [ebp+20]

segment .text

global mm_no_jump
mm_no_jump:

push ebp
mov  ebp, esp
push ebx

    ; START
    mov ecx, n

    mov edx, tab    

    mov esi, [edx]
    mov edi, [edx]

    max_min:
        cmp [edx], esi
        cmovl esi, [edx]

        cmp edi, [edx]
        cmovl edi, [edx]

        add edx, 4

        dec ecx
        cmp ecx, 0
        jg max_min

    ;     END

    mov eax, min
    mov dword[eax], esi
    mov eax, max
    mov dword[eax], edi

pop ebx
mov esp, ebp
pop ebp
ret
