void minmax_opt(int n, int * tab,  int * max,  int * min) {
    int i;
    *max = tab[0];
    *min = tab[0];
    for (i = 1; i < n; i++) {
        if (tab[i] > *max) {
            *max = tab[i];
        } else if (tab[i] < *min) {
            *min = tab[i];
        }
    }
}
