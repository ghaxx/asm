#include <cstdio>
#include <string>
#include <time.h>

using namespace std;

extern "C" void mm(int n, int * tab,  int * max,  int * min);
extern "C" void mm_no_jump(int n, int * tab,  int * max,  int * min);

extern void minmax_opt(int n, int * tab,  int * max,  int * min);
void minmax_c(int n, int * tab,  int * max,  int * min) {
    int i;
    *max = tab[0];
    *min = tab[0];
    for (i = 1; i < n; i++) {
        if (tab[i] > *max) {
            *max = tab[i];
        } else if (tab[i] < *min) {
            *min = tab[i];
        }
    }
}


void try_func(string name, void (*f)(int, int*,  int*,  int*), int rozmiar, int liczba_powtorzen, int *tab) {
   int min = 0, max = 0;
   clock_t start, stop;
   start = clock();
   for(int i=0; i<liczba_powtorzen; i++){
      (*f)(rozmiar, tab, &max, &min);
   }
   printf("%s\n", name.c_str());
   printf(" min = %d    max = %d\n", min, max); 
   stop = clock();
   printf(" time = %f  ( %d cykli)\n", (stop - start)*1.0/CLOCKS_PER_SEC, (int)(stop - start));
}

int main(){
   const int rozmiar = 2000000;
   const int liczba_powtorzen = 150; 
   int tab[rozmiar] = {1, 3, 3, -65, 3, 123, 4, 32, 342, 22, 11, 32, 44, 12, 324, 43}; 
   tab[rozmiar-1] = -1000;

   try_func("C", minmax_c, rozmiar, liczba_powtorzen, tab);
   try_func("C optimized", minmax_opt, rozmiar, liczba_powtorzen, tab);
   try_func("ASM", mm, rozmiar, liczba_powtorzen, tab);
   try_func("ASM no jumps", mm_no_jump, rozmiar, liczba_powtorzen, tab);

   return 0;
}