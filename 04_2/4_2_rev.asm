section	.text
%include "inc.asm"

global main

main:
	write "Podaj napis: "
    read  buf, input_size
	write "Odwrotnie:   "

	mov esi, 0
change:
	mov al, [buf+esi]
	cmp al, 10
	je end

	cmp al, 'a'
	jl skip
	cmp al, 'z'
	jg skip

	sub ax, 32
	mov [buf+esi], al
skip:
	inc esi
	jmp	change
end:
	mov edi, 0
rev:
	dec esi
	mov al, [buf + esi]
	mov [buf2 + edi], al
	cmp esi, 0
	je end_rev
	inc edi
	jmp rev
end_rev:

	write buf2, 100
	writeln ""
	exit

section .data
	input_size	equ	20

section .bss
	buf:	resb	100
	buf2:	resb	100

