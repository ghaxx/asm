section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    c_start
    write "Podaj tekst: "
    read buf, 100
    mov ebx, buf         ; ebx = adres tablicy array1
    mov ecx, 100

    mov dx, 0
    mov byte [i], 0
    aa_start:
        mov edx, [i]
        mov al, [buf + edx]  
        
        mov byte [a], 0
        mov ecx, 0
        mov edx, s1
        find_s1:
            mov bl, [edx]
            cmp bl, 0
            je dont_change

            cmp al, bl
            je got_s1

            inc edx
            inc ecx
            jmp find_s1

        got_s1:
        mov edx, [i]
        mov al, [s2 + ecx]  
        mov [code + edx], al
        jmp next

        dont_change:

        mov edx, [i]
        mov al, [buf + edx]  
        mov [code + edx], al

        next:
        mov edx, [i]
        add edx, 1
        mov [i], edx
        cmp ax, 10
        jne aa_start

    write "Obrocony:    "
    write code, 100
    writeln "";

    c_exit


section data:
    s1      db "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0
    s2      db "z", "y", "x", "w", "v", "u", "t", "s", "r", "q", "p", "o", "n", "m", "l", "k", "j", "i", "h", "g", "f", "e", "d", "c", "b", "a", 0

section .bss
    buf     resb 100
    code    resb 100
    a       resb 1
