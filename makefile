CPP_SRCS = $(wildcard *.cpp)
CXX_SRCS = $(wildcard *.cxx)
ASM_SRCS = $(wildcard *.asm)
C_SRCS   = $(wildcard *.c)

OBJ_DIR = ./obj

OBJS  = $(patsubst %.cxx, $(OBJ_DIR)/%.cxx.o, $(CXX_SRCS))
OBJS += $(patsubst %.cpp, $(OBJ_DIR)/%.cpp.o, $(CPP_SRCS))
OBJS += $(patsubst %.c,   $(OBJ_DIR)/%.c.o,   $(C_SRCS))
OBJS += $(patsubst %.asm, $(OBJ_DIR)/%.asm.o, $(ASM_SRCS))

FLAGS =  -O3 -msse3 -ffast-math 

GPP = g++ 
GCC = gcc 
EXE = file

all: $(OBJS)
	$(GPP) -o $(EXE) $(OBJS)

$(OBJ_DIR)/%.cpp.o: %.cpp dir
	$(GPP) $(CPFLAGS) -c $< -o $@

$(OBJ_DIR)/%.cxx.o: %.cxx dir
	$(GPP) $(FLAGS) $(CPFLAGS) -c $< -o $@

$(OBJ_DIR)/%.c.o: %.c dir
	$(GCC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR)/%.asm.o: %.asm dir
	nasm -I ../ -felf32 $< -o $@

clean:
	rm -rf $(OBJS) $(EXE)

dir:
	mkdir -p $(OBJ_DIR)

zip: $(ASM_SRCS) $(CPP_SRCS) $(C_SRCS)
	rm -rf $(EXE).zip
	zip -r $(EXE).zip $^

run:
	### RUN ###
	./$(EXE)
	### DONE ###

links:
	for i in `ls -d /home/kuba/dev/asm/*/` ; do rm "$$i"/makefile; ln -s ../makefile "$$i"/makefile; done