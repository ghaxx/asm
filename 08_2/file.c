#include <stdio.h>

extern int cube(double a, double b, double c, double *v, double *s); 

int main() {
    double a, b, c, v, s;
    printf("a b c = ");
    scanf("%lf %lf %lf", &a, &b, &c);
    cube(a, b, c, &v, &s);

    printf("v = %.6g\n", v);
    printf("s = %.6g\n", s);
}
