; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define a qword [ebp+8]
%define b qword [ebp+16]
%define c qword [ebp+24]
%define v dword [ebp+32]
%define s dword [ebp+36]

segment .data
two dw 2

segment .text
global cube
cube:

push ebp
mov ebp, esp
sub esp, 16              ; alokacja pamięci na dane lokalne procedury (disc i one_over_2a)
push ebx                 ; KONWENCJA C - musimy zachować oryginalne dane z rejestru EBX

fld a                    ; stack: a,
fld b                    ; stack: b, a
fld c                    ; stack: c, b, a
fmulp st1                ; stack: cb, a
fmulp st1                ; stack: cba


mov ebx, v               ; ładuję adres root1 do rejestru 
fstp qword [ebx]         ; ściągam ze stosu do *root1

fld a
fld b
fild word [two]
fmulp st1
fmulp st1               ; stack 4ab

fld a
fld c
fild word [two]
fmulp st1
fmulp st1               ; stack 4ac

fld b
fld c
fild word [two]
fmulp st1
fmulp st1               ; stack 4bc

faddp st1
faddp st1

mov ebx, s               ; ładuję adres root1 do rejestru 
fstp qword [ebx]         ; ściągam ze stosu do *root1


mov eax, 1               ; return 1
jmp short quit

quit:
pop ebx
mov esp, ebp
pop ebp
ret