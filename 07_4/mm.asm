section .text 

global minmax     
 
minmax:
	push   ebp
	mov    ebp, esp

	%idefine    n    [ebp+12]
	%idefine    a    [ebp+16]

	mov edx, a
	mov ecx, n

    mov [s], ecx
    mov ebx, edx
    mov [max], ebx
    mov [min], ebx
    min_max:
        mov eax, [s]
        mov ebx, [ebp + 12 + 4*eax]
        mov eax, [min]
        cmp eax, ebx
        jg not_min
        mov [min], ebx
        not_min:

        mov eax, [max]
        cmp eax, ebx
        jl not_max
        mov [max], ebx
        not_max:
        mov eax, [s]
        dec eax
        mov [s], eax
        cmp eax, 0
        jne min_max

    ; disassembled and copied
    sub   esp, 16
    mov   eax, [max]
    mov   [ebp - 8], eax
    mov   eax, [min]
    mov   [ebp - 4], eax
    mov   ecx, [ebp + 8]
   	mov   eax, [ebp - 8]
   	mov   edx, [ebp - 4]
   	mov   [ecx], eax
   	mov   [ecx + 4], edx
   	mov   eax, [ebp+8]
   	leave  
   	ret   


section .bss
	min resd 1
	max resd 1
	s   resw 1
