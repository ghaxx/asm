#include <stdio.h>
#include <math.h>

inline void dilateSSE(float *,float *,int);

int main(void)
{
    float data[400][400],header[1078];
    unsigned char ch;
    float grad[400][400],sq;
    int N=400,HL=1078;
    int i,j,k;
    FILE *strm;

    strm = fopen("circle.bmp","rb");
    for(i=0;i<HL;i++) header[i] = fgetc(strm);
    for(i=0;i<N;i++)
        for(j=0;j<N;j++) {
            data[i][j] = (float)fgetc(strm);
            grad[i][j] = 0;
        }
    fclose(strm);

    for(i=1;i<N-1;i++) dilateSSE(grad[i],data[i],N);
    for(k=1;k<20;k++) {
        for(i=0;i<N;i++) {
            for(j=0;j<N;j++) {
                data[i][j] = grad[i][j];
            }
        }
        for(i=1;i<N-1;i++) dilateSSE(grad[i],data[i],N);
    }

    strm = fopen("dum.bmp", "wb");
    for(i=0;i<HL;i++) fputc(header[i], strm);
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            fputc((unsigned char)grad[i][j], strm);
    fclose(strm);
}