; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define out dword [ebp+8]
%define b1 [ebp+12]
%define n dword [ebp+16]

segment .text
global dilateSSE
dilateSSE:

push ebp
mov ebp, esp
push ebx

mov eax, n
mov edx, n
mov ebx, b1
mov esi, out


loop:
    cmp     edx, 8
    jl      tail

    ; mov edi, a
    ; mov eax, [ebx - 4]
    ; mov [edi], eax
    ; mov eax, [ebx + 4]
    ; mov [edi+4], eax
    ; mov eax, [ebx - 1600]
    ; mov [edi+8], eax
    ; mov eax, [ebx + 1600]
    ; mov [edi+12], eax


    ; mov edi, b
    ; mov eax, dword [ebx]
    ; mov [edi], eax
    ; mov eax, dword[ebx]
    ; mov [edi+4], eax
    ; mov eax, dword[ebx]
    ; mov [edi+8], eax
    ; mov eax, dword[ebx]
    ; mov [edi+12], eax

    ; movups    xmm0, [b]
    ; movups    xmm1, [a]
    movups  xmm0, [ebx]
    mov ecx, [l]
    compare:
        mov     eax, [s + 4*(ecx-1)]
        movups  xmm1, [ebx + eax]
        maxps   xmm0, xmm1
        loop compare

    ; movups  xmm0, [ebx]
    ; movups  xmm1, [ebx-4]
    ; maxps   xmm0, xmm1
    ; movups  xmm1, [ebx+4]
    ; maxps   xmm0, xmm1
    ; movups  xmm1, [ebx-1600]
    ; maxps   xmm0, xmm1
    ; movups  xmm1, [ebx+1600]
    ; maxps   xmm0, xmm1

    ; maxps     xmm0, xmm1
    ; mov eax, a
    movups    [esi], xmm0

    add     esi, 4
    add     ebx, 4
    sub     edx, 1
    jmp     loop

tail:

done:
mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret

section .bss
    tmp resb 1
    a   resd 16
    b   resd 16

section .data
    l dd 8
    s dd 4, 4, -1600, 1600, -1596, 1596, -1604, 1604
    t dd 214.0, 140.0, 4.0, 4.0