section .text
%include "inc.asm"
extern printf

global main

main:
    c_start

    call get_int
    mov esi, eax
    call get_int
    add eax, esi
    easy_println "sum = %d", eax
    
    c_exit

get_int:
    read tmp, 4
    read_number tmp, in1
    mov eax, [in1]
    ret


section data:
    arr     db  10, 9, 11, 8, 12, 1, 20, 7, 13
    s       db  9
    i       db  0

section .bss
    tmp     resd 5
    in1     resd 1
    in2     resd 1