section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    c_start
    ; print data
    mov     esi, 20
    mov     edi, 30
    push    esi
    push    edi
    push    InputFormat
    call    printf
    pop     edx
    jmp     check

check:
    mov     edx, 0
    mov     eax, esi
    mov     ebx, edi
    ;pop     ebx
    ;pop     eax

    div     ebx


    cmp     edx, 0
    je      found

    mov     esi, ebx
    mov     edi, edx
    
    ;push    ebx
    ;push    eax
    jmp     check

found:
    pop     eax
    pop     ebx
    mul     ebx
    div     edi
    mov     edi, eax

    push    edi
    push    OutputFormat
    call    printf
    c_exit

section .data
    InputFormat     db  "nww(%d, %d) = ", 0
    OutputFormat    db  "%d",10, 0

