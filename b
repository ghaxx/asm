#!/bin/bash
nasm -f macho64 -o obj/$1.o $1.asm && ld -o obj/$1 obj/$1.o -arch x86_64 -lc -macosx_version_min 10.6 && obj/$1
