[bits 32]

section .text
global _main
extern _printf

_main:

  ;mov eax, 0xDE
  ;push eax
  push message
  call _printf
  add esp, 8

  ret

section .data
    message db "Register = %08X", 10, 0