section .text
%include "inc.asm"
extern printf

global main

main:
    c_start

    mov ecx, 0
    mov [min], ecx

    mov ecx, 0
    mov [i], ecx
    
    build_arr:
        mov ecx, [i]
        
        mov edx, ecx
        mov [j], edx
        find_next:
            mov edx, [j]
            inc edx
            mov [j], edx
            cmp j, 10
            jge proc

            mov eax, [arr + edx]
            mov ebx, [arr + ecx]
            cmp eax, ebx
            jg find_next

            mov [arr + edx], ebx
            mov [arr + ecx], eax
            jmp find_next
        proc:

        xor ecx, ecx
        mov ecx, [min]

        mov ecx, [i]
        inc ecx
        mov [i], ecx

        mov ebx, [size]

        cmp ecx, ebx
        jl build_arr

    mov ecx, 10
    mov [tmp], ecx
    print:  
        push format
        call printf
        pop eax
        pop eax ; add esp, 8 nie dziala

        mov ecx, [tmp]
        dec ecx
        mov [tmp], ecx
        loop print

    easy_println "" ; makro
    c_exit


section data:
    arr     db 7, 12, 8, 9, 21, 2, 4, 15, 7, 9, 5
    format  db "%d, ", 0
    size    db 10

section .bss
    tmp      resw 5
    work_arr resq 20
    min      resb 2
    i        resb 1
    j        resb 1
