section .text
%include "inc.asm"
extern printf

global main

main:
    c_start

    ; START
    mov byte [s], 7
    mov byte [i], 0
    mov ebx, [arr + 8*4]
    mov [max], ebx
    mov [min], ebx
    min_max:
        mov eax, [s]
        mov ebx, [arr + eax*4]
        mov eax, [min]
        cmp eax, ebx
        jg not_min
        mov [min], ebx
        not_min:

        mov eax, [max]
        cmp eax, ebx
        jl not_max
        mov [max], ebx
        not_max:

        mov eax, [s]
        dec eax
        mov [s], eax
        cmp eax, 0
        jge min_max

        ; END
        
    mov eax, [min]
    mov ebx, [max]
    easy_println "eax = %d", eax
    easy_println "ebx = %d", ebx
    c_exit


section data:
    arr     dd  -3, 9, 11, 8, 12, 1, 20, 7, 21

section .bss
    min     resb  10
    max     resb  10
    i       resb  10
    s       resb  10

; (11:38) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; ./bc: line 2:  6823 Segmentation fault      (core dumped) obj/$1
; (11:38) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; 5_1_min_max.asm:41: error: symbol `s' redefined
; (11:39) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; a
; eax = 3
; ebx = 1
; (11:39) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 0
; ebx = 0
; (11:39) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 16394
; ebx = 16394
; (11:39) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 134940938
; ebx = 134940938
; (11:39) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 2314
; ebx = 2314
; (11:40) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 10
; ebx = 10
; (11:40) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:41) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 42
; ebx = 42
; (11:41) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 42
; ebx = 42
; (11:41) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:41) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:41) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:42) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:42) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 10
; ebx = 10
; (11:42) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:42) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 18
; ebx = 18
; (11:42) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 150994944
; ebx = 150994944
; (11:46) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 150994944
; ebx = 150994944
; (11:46) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 150994944
; ebx = 150994944
; (11:46) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; 5_1_min_max.asm:18: error: beroset-p-650-invalid effective address
; (11:47) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; 5_1_min_max.asm:18: error: expression syntax error
; (11:47) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; 5_1_min_max.asm:18: error: expression syntax error
; (11:47) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; 5_1_min_max.asm:18: error: expression syntax error
; (11:47) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; 5_1_min_max.asm:18: error: expression syntax error
; (11:47) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 10
; ebx = 10
; (11:48) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 134217728
; ebx = 134217728
; (11:48) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 0
; ebx = 0
; (11:48) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 990059265
; ebx = 990059265
; (11:48) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 20
; ebx = 20
; (11:48) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 5405185
; ebx = 5405185
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 20
; ebx = 20
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = -68
; ebx = -68
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 572797482
; ebx = 572797482
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 0
; ebx = 0
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 150994944
; ebx = 150994944
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 589824
; ebx = 589824
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 2304
; ebx = 2304
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 9
; ebx = 9
; (11:49) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 13
; ebx = 13
; (11:50) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 150994944
; ebx = 150994944
; (11:50) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 9
; ebx = 9
; (11:50) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 13
; ebx = 13
; (11:50) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 9
; ebx = 9
; (11:50) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 10
; ebx = 10
; (11:51) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 20
; ebx = 1
; (11:53) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 21
; ebx = 1
; (11:56) kuba@kuba:~/dev/asm$ ./bc 5_1_min_max
; eax = 21
; ebx = -3
; (11:56) kuba@kuba:~/dev/asm$ 
