section .text 

global fac     

fac:
    push   ebp                  
    mov    ebp, esp            

    %idefine    a    [ebp+8]
    %idefine    n    [ebp+12]

    mov edx, a
    mov ecx, n
    mov eax, 1

    lp:
        mul byte [edx + 4*(ecx-1)]
        loop lp

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret
