#include <stdio.h>
#include <malloc.h>

int fac (int* a, int b); 

int main() {
     int n = 0, i = 0;
     int *a;
     printf("n = ");
     scanf("%d", &n);

     a = malloc(n * sizeof (int));

     while(++i <= n) {
         a[i-1] = i;
     }

     printf("%d! = %d\n", n, fac(a, n));
     return 0;
}
