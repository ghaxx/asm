section	.text			; początek sekcji kodu.
%include "inc.asm"

global _main		        ; linux rozpoczyna wykonywanie programu od etykiety _start
                                 ; musi ona być widoczna na zewnątrz (global)

_main:				        ; punkt startu programu
	mov	rax, 4      		; numer funkcji systemowej:
				            ; sys_write - zapisz do pliku
	mov	rdi, 1		        ; numer pliku, do którego piszemy.
				            ; 1 = standardowe wyjście = ekran
	mov	rsi, tekst	        ; RSI = adres (offset) tekstu
	mov	rdx, dlugosc	    ; RDX = długość tekstu
	syscall			        ; wywołujemy funkcję systemową
	mov	rax, 1      		; numer funkcji systemowej
				            ; (sys_exit - wyjdź z programu)
	syscall			        ; wywołujemy funkcję systemową

section .data			    ; początek sekcji danych.
	tekst	db	"Czesc", 0ah	; nasz napis, który wyświetlimy
	dlugosc	equ	$ - tekst	; długość napisu

