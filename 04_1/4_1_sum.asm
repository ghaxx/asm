section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    c_start

    write "podaj ilosc liczb: "
    read buf, 10
    read_number buf, s

    mov word[i], 0
    read_numbers:
        mov ebx, [i]        ; increment counter
        inc ebx
        mov [a+ebx-1], ebx
        mov [i], ebx
        cmp ebx, [s]
        jne read_numbers

    mov ebx, a              ; ebx = adres tablicy
    mov dx, 0               ; dx będzie zawierać sumę
    mov ah, 0               ; zerujemy ah, bo w sumowaniu uczestniczy rejestr ax
    mov ecx, [s]

    lp:
    mov al, [ebx]           ; al = *ebx
    add dx, ax              ; dx += ax (nie al!)
    inc ebx                 ; bx++
    loop lp

    easy_println "sum = %d", dx

    c_exit


section data:

section .bss
    buf     resw 8
    a       resw 200
    i       resw 8
    s       resw 100
