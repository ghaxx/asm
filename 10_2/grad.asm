; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define out dword [ebp+8]
%define b1 [ebp+12]
%define n dword [ebp+16]

segment .text
global gradientSSE
gradientSSE:

push ebp
mov ebp, esp
push ebx

mov eax, n
mov edx, n
mov ebx, b1
mov esi, out

loop:
    cmp     edx, 4
    jl      tail
    movups    xmm0, [ebx + 4]
    movups    xmm1, [ebx - 4]
    subps   xmm0, xmm1
    mulps  xmm0, xmm0

    movups    xmm1, [ebx + eax*4]
    movups    xmm2, [ebx - 1600]
    subps   xmm1, xmm2
    mulps  xmm1, xmm1

    addps xmm0, xmm1

    sqrtps xmm0, xmm0
    movups    [esi], xmm0
    add     esi, 16
    add     ebx, 16
    sub     edx, 4
    jmp     loop

tail:

done:
mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret

section .bss
    tmp resb 1
