section	.text
%include "inc.asm"
global main

main:
    write "Time: "
    mov eax, sys_time
    syscall
    mov esi, eax

    ;mov rbx, rcx
    write_number esi, 60
    writeln "|"

    exit

go:
    ;push 5
    ;push rbp
    ;mov rbp, rsp
    ;mov rsi, rbp
    ;add rsi, 8
    ;add byte [rsi], '0'
    ;write rsi, 1

write_number 5, 1
    writeln ""
write_number 11, 2
    writeln ""
write_number 6, 1
    writeln ""
write_number 30, 2
    writeln ""
write_number 7, 1

    ;time rcx
    ;push rcx
    ;push rbp
    ;mov rbp, rsp
    ;mov rsi, rbp
    ;add rsi, 8
    ;mov rsi, 5
    add byte [esi], '0'
    write esi, 10

    writeln ""

    exit

section .data

section .bss
	buf:	resb	255

