%define a dword [ebp+8]
%define b dword [ebp+12]
%define x qword [ebp+16]
%define n dword [ebp+24]

segment .text
global oblicz

oblicz:

push ebp
mov ebp, esp
push ebx

fld x
fild n
fmulp st1
fsin        ; sin(nx)

fld b
fmulp st1    ; b * sin(xn)

fld x
fld x
fmulp st1     ; x^2
fld a
fmulp st1       ;ax^2

fadd to st1

fstp qword [ebx]
mov eax, ebx


pop ebx
mov esp, ebp
pop ebp
ret
