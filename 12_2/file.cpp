#include <cstdio>
#include <time.h>

using namespace std;

void solve(int n, float * A, float * B, float * X);

void solve_c(int n, float * a, float * b, float * x) {
  int i;
  for (i = 0; i < n; i++) {
    if (a[i] == 0 && b[i] == 0) {
      x[i] = 1000;
    } else if (a[i] == 0 && b[i] != 0) {
      x[i] = -1;
    } else {
      x[i] = b[i] / a[i];
    }
  }
}

int main(){
 const int n = 4;
 float a[] = {0, 0, 1, 2};
 float x[n];
 float b[] = {0, 1, 1, 5};

 solve_c(n, a, b, x);
 int i;
  for (i = 0; i < n; i++) {
    printf("%3g * ", a[i]);
    if (x[i] == 1000) {
      printf("Inf");
    } else if (x[i] == -1) {
      printf("NaN");
    } else {
      printf("%3g", x[i]);
    }
    printf(" = %3g\n", b[i]);
  }

   return 0;
}