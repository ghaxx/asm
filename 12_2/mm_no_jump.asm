%define n   dword [ebp+8]
%define tab dword [ebp+12]
%define max dword [ebp+16]
%define min dword [ebp+20]

segment .text

global mm_no_jump
mm_no_jump:

push ebp
mov  ebp, esp
push ebx

    ; START
    mov ecx, n

    mov edx, tab    

    mov esi, [edx]
    mov edi, [edx]

    max_min:
        mov ebx, [edx + ecx*4 - 4]
        cmp ebx, esi
        cmovl esi, ebx

        cmp edi, ebx
        cmovl edi, ebx

        loop max_min

    ;     END
    mov eax, min
    mov dword[eax], esi
    mov eax, max
    mov dword[eax], edi


mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret
