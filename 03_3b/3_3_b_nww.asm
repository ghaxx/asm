section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    ; print data
    mov     esi, a
    mov     edi, b
    push    esi
    push    edi
    push    InputFormat
    call    printf
    pop     edx
    jmp     check

check:
    mov     edx, 0
    mov     eax, esi
    mov     ebx, edi
    div     ebx

    cmp     edx, 0
    je      nww

    mov     esi, ebx
    mov     edi, edx
    jmp     check

nww:
    pop     eax
    pop     ebx
    mul     ebx ; eax := ebx * eax

    push    eax
    push    NumberFormat
    call    printf  
    easy_print " / "
    push    edi
    push    NumberFormat
    call    printf  
    
    easy_print " = "

    div     edi
    mov     edi, eax

    push    edi
    push    NumberFormat
    call    printf
    easy_println ""

    exit

section .data
    InputFormat     db  "nww(%1$d, %2$d) = %1$d*%2$d / nwd(%1$d, %2$d) = ", 0
    NumberFormat    db  "%d", 0
    EOL             db  "", 10, 0
    a               equ 5
    b               equ 30
