#include <stdio.h>

// char * dodaj(char * tab1, char * tab2, int n);

void mmx_copy(char *out, char *bufor1, int n);

int main() {
    int i, n = 21;

    char *src, *dst;
    src = new char[n];
    dst = new char[n];

    for (i = 0; i < n; i++) {
        src[i] = i + 2;
        dst[i] = i / 2;
    }

    for (i = 0; i < n; i++) {
        printf("%4d ", src[i]);
    }
    printf("\n");
    for (i = 0; i < n; i++) {
        printf("%4d ", dst[i]);
    }
    printf("\n");
    // dst += 4;
    asm (
            "movq    %%mm0,  %0;"
            "movq    %%mm1,  %1;"
            // "paddsb  %%mm0,  %%mm1;"
            "movq    %0, %%mm1;"
    :: "m"(dst), "m"(src)
    );
    // mmx_copy(dst, src, n);
    printf("dodaj %d\n", i);
    // char *r = dodaj(src, dst, n)
    for (i = 0; i < n; i++) {
        printf("%4d ", dst[i]);
    }
    printf("\n");
}
