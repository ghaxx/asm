%define out dword [ebp+8]
%define b1 [ebp+12]
%define n dword [ebp+16]

segment .text

global mmx_copy
mmx_copy:

push ebp
mov ebp, esp
push ebx

mov edx, n
mov ecx, b1
mov eax, out

loop:
    cmp     edx, 8
    jl      tail
    movq    mm0, qword [ecx]
    paddsb  mm0, qword [eax]
    movq    qword[eax], mm0
    add     ecx, 8
    add     eax, 8
    sub     edx, 8
    jmp     loop

tail:
    cmp     edx, 0
    je      done
    mov     byte[eax], 10
    inc     ecx
    inc     eax
    dec     edx
    jmp     tail

done:
mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret
