section .text
%include "inc.asm"
extern printf

global main
; nwd(a, b) = nwd(a, a % b), nwd(a, 0) = a
; div ebx     ; Divides eax / ebx => ebx*eax + edx

main:
    ; print data
    mov     esi, 1
    mov     edi, 60

check:
    mov     edx, 0
    mov     eax, edi
    mov     ebx, esi
    div     ebx

    cmp     edx, 0
    jne     skip

    push    esi
    push    OutputFormat
    call    printf
skip:
    add     esi, 1
    cmp     esi, edi
    jne     check

    exit

section .data
    OutputFormat    db  "%d",10, 0

