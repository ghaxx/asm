section	.text			
%include "inc.asm"

global main

main:
	open_by_name "g"
	mov ebx, eax
	cmp	ebx, 1
	jg	_write
	exit		

_write:
	write_file "Czesc", ebx
	close_file
	write ebx, 10
	exit		

section .data

section .bss
	buf:	resb	1

