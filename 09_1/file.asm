%define a dword [ebp+8]
%define b dword [ebp+12]

segment .text
global _Z6rotateji
global _Z6rotatehi

_Z6rotateji:

push ebp
mov ebp, esp
sub esp, 16              ; alokacja pamięci na dane lokalne procedury (disc i one_over_2a)
push ebx                 ; KONWENCJA C - musimy zachować oryginalne dane z rejestru EBX

mov eax, a              
mov ecx, b
lp:
shl eax, 1
loop lp

pop ebx
mov esp, ebp
pop ebp
ret


_Z6rotatehi:

push ebp
mov ebp, esp
sub esp, 16              ; alokacja pamięci na dane lokalne procedury (disc i one_over_2a)
push ebx                 ; KONWENCJA C - musimy zachować oryginalne dane z rejestru EBX

mov eax, a
mov ecx, b
lp2:
shl eax, 1
loop lp2

pop ebx
mov esp, ebp
pop ebp
ret
