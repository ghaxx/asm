#include <stdio.h>

unsigned int rotate(unsigned int x, int n = 1);

unsigned char rotate(unsigned char x, int n = 1);

int main() {
    unsigned int i = 2;
    unsigned char c = 3;
    printf("%d\n", rotate(i, 2));
    printf("%d\n", rotate(c, 3));
}
