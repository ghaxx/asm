; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define out dword [ebp+8]
%define b1 [ebp+12]
%define b2 [ebp+16]
%define n dword [ebp+20]

segment .text
global mmx_copy
mmx_copy:

push ebp
mov ebp, esp
push ebx

mov edx, n
mov ecx, b2
mov ebx, b1
mov eax, out

loop:
    cmp     edx, 8
    jl      tail
    movq    mm0, qword [ecx]
    movq    qword[eax], mm0
    add     ecx, 8
    add     eax, 8
    sub     edx, 8
    jmp     loop

tail:
    cmp     edx, 0
    je      done
    mov     byte[eax], 10
    inc     ecx
    inc     eax
    dec     edx
    jmp     tail

done:
mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret
