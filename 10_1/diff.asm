; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define out dword [ebp+8]
%define b1 [ebp+12]
%define b2 [ebp+16]
%define n dword [ebp+20]

segment .text
global diff
diff:

push ebp
mov ebp, esp
push ebx

mov edx, n
mov ecx, b2
mov ebx, b1
mov esi, out

loop:
    cmp     edx, 8
    jl      tail
    movq    mm0, qword [ecx]
    psubb   mm0, qword [ebx]
    movq    qword[esi], mm0
    add     ecx, 8
    add     esi, 8
    add     ebx, 8
    sub     edx, 8
    jmp     loop

tail:
    cmp     edx, 0
    je      done
    mov     ah, byte[ecx]
    sub     ah, byte[ebx]
    mov     byte[esi], ah
    inc     ecx
    inc     ebx
    inc     esi
    dec     edx
    jmp     tail

done:
mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret

section .bss
    tmp resb 1