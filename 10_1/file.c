#include <stdio.h>

#define N 19

void diff(char *out, char *bufor1, char *bufor2, int n);

int main(void)
{
    char tablica[N], d[N];
    int i;

    for(i = 0; i < N; i++)
        tablica[i] = N / 2 - i;

    diff(d+1, tablica + 1 , tablica, N-1);

    for (i = 0; i < N; i++)
        printf("%4d ", tablica[i]);

    printf("\n");

    for (i = 0; i < N; i++)
        printf("%4d ", d[i]);

    printf("\n");
}