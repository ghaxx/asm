section .text
%include "inc/inc.asm"

global main

main:
    mov ebx, 1
    mov ecx, 1999
    jmp check

check:
    ; mov eax, 3992003
    mov eax, ecx
    mov edx, 0    
    inc ebx
    cmp ebx, eax
    jge is_prime

    div ebx     ; Divides eax / ebx => ebx*eax + edx
    cmp edx, 0
    je is_not_prime
    jmp check

is_prime:
    writeln "Is prime"
    exit

is_not_prime:
    writeln "Is not prime"
    exit

section .data
    input_size  equ 20

section .bss
    buf:    resb    1

