section	.text
%include "inc.asm"

global main

main:
	c_start
	write "Podaj imie: "
	jmp 	_read

_read:
    read    buf, input_size
	write "Czesc, "
    write buf, input_size
	jmp done
done:
	c_exit

section .data
	input_size	equ	20

section .bss
	buf:	resb	1

