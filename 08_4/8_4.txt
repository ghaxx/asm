====================================
== ASM:



%define x_min qword [ebp+8]
%define x_max qword [ebp+16]
%define step dword [ebp+24]
%define a qword [ebp+28]
%define b qword [ebp+36]
%define P qword [ebp+44]
%define Q qword [ebp+52]
%define r dword [ebp+60]

segment .data
d180 dw 180
d1 dw 1

segment .bss
x   resq 4
i   resw 4

segment .text
global prec
prec:

push ebp
mov ebp, esp
sub esp, 16              ; alokacja pamięci na dane lokalne procedury (disc i one_over_2a)
push ebx                 ; KONWENCJA C - musimy zachować oryginalne dane z rejestru EBX

mov ecx, step
dec ecx
mov [i], ecx

mov ecx, step
lp:
    fld x_max
    fld x_min
    fsubp st1
    fild step
    fild dword [d1]
    fsubp st1
    fdiv
    fild dword [i]
    fmulp st1
    fld x_min
    faddp st1   ; x

    mov ebx, x              
    ; a*(sin(P*pi*x/180.0))^2 + b*(sin(Q*pi*x/180.0))^2
    fstp qword [x]

    fld qword [x]
    fldpi
    fld Q
    fmulp st1
    fmulp st1
    fild word [d180]
    fdivp st1
    fsin
    fmul st0
    fld b
    fmulp st1

    fld qword [x]
    fldpi
    fld P
    fmulp st1
    fmulp st1
    fild word [d180]
    fdivp st1
    fsin
    fmul st0
    fld a
    fmulp st1

    faddp st1

    mov ebx, r
    fstp qword [ebx + 8*(ecx - 1)]      

    mov ebx, [i]
    dec ebx
    mov [i], ebx 

    dec ecx
    jnz lp

mov eax, 1               ; return 1

pop ebx
mov esp, ebp
pop ebp
ret
====================================
== C:

#include <stdio.h>

//a*(sin(P*pi*x/180.0))^2 + b*(sin(Q*pi*x/180.0))^2
extern int prec(double x_min, double x_max, int step, double a, double b, double P, double Q, double *r); 

int main() {
    int i, step = 11;
    double x_min = 0, x_max = 180, a = 1, b = 1, P = 1, Q = 1;
    // printf("x_min x_max step a b P Q = ");
    // scanf("%lf %lf %d %lf %lf %lf %lf", &x_min, &x_max, &step, &a, &b, &P, &Q);
    double r[step];

    prec(x_min, x_max, step, a, b, P, Q, r);
    printf("precalculated\n");

    for (i = 0; i < step; i++) {
        printf("r[%.6f] = %.6f\n", x_min + ((x_max - x_min) / (step - 1)) * i, r[i]);
        // b[i] = 3;
    }
}
