#include <stdio.h>

//a*(sin(P*pi*x/180.0))^2 + b*(sin(Q*pi*x/180.0))^2
extern int prec(double x_min, double x_max, int step, double a, double b, double P, double Q, double *r); 

int main() {
    int i, step = 11;
    double x_min = 0, x_max = 180, a = 1, b = 1, P = 1, Q = 1;
    // printf("x_min x_max step a b P Q = ");
    // scanf("%lf %lf %d %lf %lf %lf %lf", &x_min, &x_max, &step, &a, &b, &P, &Q);
    double r[step];

    prec(x_min, x_max, step, a, b, P, Q, r);
    printf("precalculated\n");

    for (i = 0; i < step; i++) {
        printf("r[%.6f] = %.6f\n", x_min + ((x_max - x_min) / (step - 1)) * i, r[i]);
        // b[i] = 3;
    }
}
