; funkcja quadratic
; znajduje pierwiastki równania kwadratowego:
; a*x^2 + b*x + c = 0
; prototyp C:
; int quadratic( double a, double b, double c,
; double * root1, double *root2 )
; Parametry:
; a, b, c - współczynniki równania
; root1 - wskaźnik do zmiennej typu double, przechowującej rozwiązanie
; root2 - j.w.
; Wartość zwracana:
; 1 jeśli istnieją pierwiastki rzeczywiste, w przeciwnym wypadku 0

%define l dword [ebp+8]
%define a dword [ebp+12]
%define b dword [ebp+16]
%define d dword [ebp+20]

segment .data
zero dw 0

segment .text
global vec
vec:

push ebp
mov ebp, esp
sub esp, 16              ; alokacja pamięci na dane lokalne procedury (disc i one_over_2a)
push ebx                 ; KONWENCJA C - musimy zachować oryginalne dane z rejestru EBX

fild word [zero]
mov ecx, l
add_a:
    mov eax, a
    fld qword [eax + 8*(ecx-1)]
    mov eax, b
    fld qword [eax + 8*(ecx-1)]
    fmulp st1
    faddp st1
    loop add_a


mov ebx, d               ; ładuję adres d do rejestru 
fstp qword [ebx]         ; ściągam ze stosu do *d

mov eax, 1               ; return 1
jmp short quit

quit:
pop ebx
mov esp, ebp
pop ebp
ret