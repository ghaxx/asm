#include <stdio.h>

extern int vec(int l, double *a, double *b, double *d); 

int main() {
    int l, i;
    double d = -3.14;
    printf("l = ");
    scanf("%d", &l);

    printf("vector a\n");
    double a[l];

    for (i = 0; i < l; i++) {
        printf("a[%d] = ", i);
        scanf("%lf", a+i);
        // a[i] = 2;
    }

    printf("vector b\n");
    double b[l];

    for (i = 0; i < l; i++) {
        printf("b[%d] = ", i);
        scanf("%lf", b+i);
        // b[i] = 3;
    }

    vec(l, a, b, &d);
    printf("a dot b = %.6g\n", d);
}
