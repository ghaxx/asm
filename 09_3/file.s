	.file	"file.cpp"
	.section	.text._ZNSt8ios_base5widthEi,"axG",@progbits,_ZNSt8ios_base5widthEi,comdat
	.align 2
	.weak	_ZNSt8ios_base5widthEi
	.type	_ZNSt8ios_base5widthEi, @function
_ZNSt8ios_base5widthEi:
.LFB624:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	-4(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE624:
	.size	_ZNSt8ios_base5widthEi, .-_ZNSt8ios_base5widthEi
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZN6BigIntC2Ei,"axG",@progbits,_ZN6BigIntC5Ei,comdat
	.align 2
	.weak	_ZN6BigIntC2Ei
	.type	_ZN6BigIntC2Ei, @function
_ZN6BigIntC2Ei:
.LFB972:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %edx
	movl	8(%ebp), %eax
	movl	%edx, (%eax)
	movl	12(%ebp), %eax
	cmpl	$532676608, %eax
	ja	.L4
	sall	$2, %eax
	jmp	.L5
.L4:
	movl	$-1, %eax
.L5:
	movl	%eax, (%esp)
	call	_Znaj
	movl	8(%ebp), %edx
	movl	%eax, 4(%edx)
	movl	12(%ebp), %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_Z5zerujPij
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE972:
	.size	_ZN6BigIntC2Ei, .-_ZN6BigIntC2Ei
	.weak	_ZN6BigIntC1Ei
	.set	_ZN6BigIntC1Ei,_ZN6BigIntC2Ei
	.section	.text._ZN6BigIntD2Ev,"axG",@progbits,_ZN6BigIntD5Ev,comdat
	.align 2
	.weak	_ZN6BigIntD2Ev
	.type	_ZN6BigIntD2Ev, @function
_ZN6BigIntD2Ev:
.LFB979:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	je	.L6
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_ZdaPv
.L6:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE979:
	.size	_ZN6BigIntD2Ev, .-_ZN6BigIntD2Ev
	.weak	_ZN6BigIntD1Ev
	.set	_ZN6BigIntD1Ev,_ZN6BigIntD2Ev
	.text
	.align 2
	.globl	_ZN6BigInt5dodajEi
	.type	_ZN6BigInt5dodajEi, @function
_ZN6BigInt5dodajEi:
.LFB981:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	12(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_Z6dodaj2iPii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE981:
	.size	_ZN6BigInt5dodajEi, .-_ZN6BigInt5dodajEi
	.align 2
	.globl	_ZN6BigInt6pomnozEi
	.type	_ZN6BigInt6pomnozEi, @function
_ZN6BigInt6pomnozEi:
.LFB982:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	12(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_Z7pomnoz2iPii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE982:
	.size	_ZN6BigInt6pomnozEi, .-_ZN6BigInt6pomnozEi
	.align 2
	.globl	_ZN6BigInt14podzielZResztaEi
	.type	_ZN6BigInt14podzielZResztaEi, @function
_ZN6BigInt14podzielZResztaEi:
.LFB983:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	12(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_Z8podziel2iPii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE983:
	.size	_ZN6BigInt14podzielZResztaEi, .-_ZN6BigInt14podzielZResztaEi
	.section	.rodata
.LC0:
	.string	"|"
	.text
	.globl	_ZlsRSoRK6BigInt
	.type	_ZlsRSoRK6BigInt, @function
_ZlsRSoRK6BigInt:
.LFB984:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	subl	$1, %eax
	movl	%eax, -12(%ebp)
	jmp	.L15
.L16:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	subl	$12, %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	$48, 4(%esp)
	movl	%eax, (%esp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4fillEc
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	subl	$12, %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	$10, 4(%esp)
	movl	%eax, (%esp)
	call	_ZNSt8ios_base5widthEi
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_ZNSolsEj
	movl	$.LC0, 4(%esp)
	movl	%eax, (%esp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	subl	$1, -12(%ebp)
.L15:
	cmpl	$0, -12(%ebp)
	jns	.L16
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE984:
	.size	_ZlsRSoRK6BigInt, .-_ZlsRSoRK6BigInt
	.section	.rodata
.LC1:
	.string	"\n"
.LC2:
	.string	"dodaj"
.LC3:
	.string	"pomnoz"
.LC4:
	.string	"podziel"
.LC5:
	.string	" r = "
	.text
	.globl	main
	.type	main, @function
main:
.LFB985:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA985
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	andl	$-16, %esp
	subl	$32, %esp
	.cfi_offset 3, -12
	movl	$6, 4(%esp)
	leal	24(%esp), %eax
	movl	%eax, (%esp)
.LEHB0:
	call	_ZN6BigIntC1Ei
.LEHE0:
	leal	24(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$_ZSt4cout, (%esp)
.LEHB1:
	call	_ZlsRSoRK6BigInt
	movl	$.LC1, 4(%esp)
	movl	%eax, (%esp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$.LC2, (%esp)
	call	puts
	movl	$0, 16(%esp)
	jmp	.L18
.L19:
	movl	$-1, 4(%esp)
	leal	24(%esp), %eax
	movl	%eax, (%esp)
	call	_ZN6BigInt5dodajEi
	leal	24(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$_ZSt4cout, (%esp)
	call	_ZlsRSoRK6BigInt
	movl	$.LC1, 4(%esp)
	movl	%eax, (%esp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addl	$1, 16(%esp)
.L18:
	cmpl	$7, 16(%esp)
	jle	.L19
	movl	$.LC3, (%esp)
	call	puts
	movl	$0, 16(%esp)
	jmp	.L20
.L21:
	movl	$512, 4(%esp)
	leal	24(%esp), %eax
	movl	%eax, (%esp)
	call	_ZN6BigInt6pomnozEi
	leal	24(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$_ZSt4cout, (%esp)
	call	_ZlsRSoRK6BigInt
	movl	$.LC1, 4(%esp)
	movl	%eax, (%esp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addl	$1, 16(%esp)
.L20:
	cmpl	$7, 16(%esp)
	jle	.L21
	movl	$.LC4, (%esp)
	call	puts
	movl	$0, 16(%esp)
	jmp	.L22
.L23:
	movl	$512, 4(%esp)
	leal	24(%esp), %eax
	movl	%eax, (%esp)
	call	_ZN6BigInt14podzielZResztaEi
	movl	%eax, 20(%esp)
	leal	24(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$_ZSt4cout, (%esp)
	call	_ZlsRSoRK6BigInt
	movl	$.LC5, 4(%esp)
	movl	%eax, (%esp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	20(%esp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ZNSolsEi
	movl	$.LC1, 4(%esp)
	movl	%eax, (%esp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addl	$1, 16(%esp)
.L22:
	cmpl	$7, 16(%esp)
	jle	.L23
	movl	$10, (%esp)
	call	putchar
.LEHE1:
	leal	24(%esp), %eax
	movl	%eax, (%esp)
	call	_ZN6BigIntD1Ev
	movl	$0, %eax
	jmp	.L27
.L26:
	movl	%eax, %ebx
	leal	24(%esp), %eax
	movl	%eax, (%esp)
	call	_ZN6BigIntD1Ev
	movl	%ebx, %eax
	movl	%eax, (%esp)
.LEHB2:
	call	_Unwind_Resume
.LEHE2:
.L27:
	movl	-4(%ebp), %ebx
	leave
	.cfi_restore 5
	.cfi_restore 3
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE985:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA985:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE985-.LLSDACSB985
.LLSDACSB985:
	.uleb128 .LEHB0-.LFB985
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB985
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L26-.LFB985
	.uleb128 0
	.uleb128 .LEHB2-.LFB985
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE985:
	.text
	.size	main, .-main
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB994:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$1, 8(%ebp)
	jne	.L28
	cmpl	$65535, 12(%ebp)
	jne	.L28
	movl	$_ZStL8__ioinit, (%esp)
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, 8(%esp)
	movl	$_ZStL8__ioinit, 4(%esp)
	movl	$_ZNSt8ios_base4InitD1Ev, (%esp)
	call	__cxa_atexit
.L28:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE994:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I__ZN6BigInt5dodajEi, @function
_GLOBAL__sub_I__ZN6BigInt5dodajEi:
.LFB995:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$65535, 4(%esp)
	movl	$1, (%esp)
	call	_Z41__static_initialization_and_destruction_0ii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE995:
	.size	_GLOBAL__sub_I__ZN6BigInt5dodajEi, .-_GLOBAL__sub_I__ZN6BigInt5dodajEi
	.section	.init_array,"aw"
	.align 4
	.long	_GLOBAL__sub_I__ZN6BigInt5dodajEi
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
