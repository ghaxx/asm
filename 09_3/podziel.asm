%define l dword [ebp+8]
%define a dword [ebp+12]
%define n dword [ebp+16]

segment .text
global _Z8podziel2iPii

_Z8podziel2iPii:

push ebp
mov ebp, esp
sub esp, 16          
push ebx             

mov ecx, l
mov eax, a
mov ebx, n

mov edx, 0
mov edi, a

lp:
    mov eax, [edi+4*(ecx-1)]
    div ebx
    mov [edi+4*(ecx-1)], eax

    dec ecx
    cmp ecx, 0
    jge lp


mov eax, edx

pop ebx
mov esp, ebp
pop ebp
ret

