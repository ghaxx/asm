%define l dword [ebp+8]
%define a dword [ebp+12]
%define n dword [ebp+16]

segment .text

global _Z7pomnoz2iPii

_Z7pomnoz2iPii:

push ebp
mov ebp, esp
sub esp, 16
push ebx

mov esi, 1
mov edi, a
mov ebx, n

mov eax, [edi]
mul ebx
mov [edi], eax
mov ecx, edx
clc
lp2:
    mov eax, [edi+4*esi]
    mul ebx
    add eax, ecx
    mov [edi+4*esi], eax
    mov ecx, edx
    inc esi
    cmp l, esi
    jne lp2

quit2:
mov eax, 1
jmp done2

overflow2:
mov eax, 0
jmp done2

done2:
pop ebx
mov esp, ebp
pop ebp
ret