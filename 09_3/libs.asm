%define a dword [ebp+8]
%define b dword [ebp+12]
%define c dword [ebp+16]

segment .data
zero dw 0

segment .text

global _Z6kopiujPiS_j
_Z6kopiujPiS_j:

push ebp
mov ebp, esp
sub esp, 16            
push ebx               

mov esi, a
mov edi, b
mov ecx, c
rep movsd

mov eax, 1

pop ebx
mov esp, ebp
pop ebp
ret

global _Z5zerujPij
_Z5zerujPij:

push ebp
mov ebp, esp
sub esp, 16          
push ebx               

mov eax, 0
mov edi, a
mov ecx, b
rep stosd

pop ebx
mov esp, ebp
pop ebp
ret