#include <stdio.h>
#include <iostream>

using namespace std;

int dodaj2(int l, int * cel, int n);
int pomnoz2(int l, int * cel, int n);
int podziel2(int l, int * cel, int n);

void kopiuj(int * cel, int * zrodlo, unsigned int n);
void zeruj(int * cel, unsigned int n);

class BigInt{
  unsigned int rozmiar; 
  int * dane;
public:
  explicit BigInt(int rozmiar) 
    : rozmiar(rozmiar),
      dane( new int[rozmiar] ){
    zeruj(dane, rozmiar);
  }

  BigInt(const BigInt & x) 
    :  rozmiar(x.rozmiar),
       dane(new int[x.rozmiar]){ 
    kopiuj(dane, x.dane, x.rozmiar);
  }  

  BigInt & operator=(const BigInt & x) {
    if(rozmiar != x.rozmiar){
      int * tmp = new int[x.rozmiar];
      delete[] dane; 
      rozmiar = x.rozmiar;
      dane = tmp;
    }
    kopiuj(dane, x.dane, x.rozmiar);
    return *this;
  }

  ~BigInt(){        
    delete[] dane;
  }
  
// do zaimplementowania w zadaniu 3
  int dodaj(int n);
  int pomnoz(int n);
  int podzielZReszta(int n);
  
  // BigInt & operator=(const char * liczba);
  friend std::ostream & operator << (std::ostream & str, const BigInt & x);

// do zaimplementowania w zadaniu 4
  // friend BigInt operator+ (const BigInt & a, const BigInt & b);
  // friend BigInt operator- (const BigInt & a, const BigInt & b);
  
};

int BigInt::dodaj(int n) {
    return dodaj2(rozmiar, dane, n);
}

int BigInt::pomnoz(int n) {
    return pomnoz2(rozmiar, dane, n);
}

int BigInt::podzielZReszta(int n) {
    return podziel2(rozmiar, dane, n);
//return 8;
}

std::ostream & operator << (std::ostream & str, const BigInt & x) {
    int i;
    for (i = x.rozmiar - 1; i >= 0; i--) {
        str.fill('0');
        str.width(10);
        str << (unsigned int) x.dane[i] << "|";
    }
}

int main() {
    BigInt a(6);
    std::cout << a << "\n";
    int i;
    printf("dodaj\n");
    for (i = 0; i < 8; i++) {
        a.dodaj((2<<30));
//        a.dodaj(1);
        std::cout << a << "\n";
    }

    printf("pomnoz\n");
    for (i = 0; i < 8; i++) {
        a.pomnoz(2<<8);
        std::cout << a << "\n";
    }

    printf("podziel\n");
    for (i = 0; i < 8; i++) {
        int c = a.podzielZReszta(2<<8);
        std::cout << a << " r = " << c << "\n";
    }
    printf("\n");
}
