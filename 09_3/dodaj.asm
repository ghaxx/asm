%define l dword [ebp+8]
%define a dword [ebp+12]
%define n dword [ebp+16]

segment .text
global _Z6dodaj2iPii

_Z6dodaj2iPii:

push ebp
mov ebp, esp
sub esp, 16          
push ebx             

mov ecx, 1
mov eax, a
mov ebx, n
add dword [eax], ebx
lp:
    adc dword [eax + 4 * ecx], 0
    inc ecx
    cmp l, ecx
    jne lp

jc overflow

quit:
mov eax, 1
jmp done

overflow:
mov eax, 0
jmp done

done:
pop ebx
mov esp, ebp
pop ebp
ret